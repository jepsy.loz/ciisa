/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

/**
 *
 * @author eahumada
 */
class Comprador {
    String rut;    
    String nombreComprador;
    String apellidoComprador;
    
    //metodos setters
    public void setRut(String rut){
        this.rut=rut;
    }
    public void setNombreComprador(String nombreComprador){
        this.nombreComprador=nombreComprador;
    }
    public void setApellidoComprador(String apellidoComprador){
        this.apellidoComprador=apellidoComprador;
    }
    
    //metodos getters
    
    public String getRut(){
        return this.rut;
    }
    public String getNombreComprador(){
        return this.nombreComprador;
    }
    public String getApellidoComprador(){
        return this.apellidoComprador;
    }
}
