/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

/**
 *
 * @author eahumada
 */
public class Vehiculo {
    int  codigo;
    String marca;
    String tipo;
    String modelo;
    int anio;
    double kilometraje;
    String patente;
    
    //metodos setters
    public setCodigo(int codigo){
        this.codigo = codigo;
    }
    public setMarca(String marca){
        this.marca=marca;
    }
    public setTipo(String tipo){
        this.tipo=tipo;
    }
    public setModelo(String modelo){
        this.modelo=modelo;
    }
    public setAnio(int anio){
        this.anio=anio;
    }
    public setKilometraje(double kilometraje){
        this.kilometraje=kilometraje;
    }
    public setPatente(String patente){
        this.patente=patente;
    }
    
    //metodos getters
    public getCodigo(){
        return this.codigo;
    }
    public getMarca(){
        return this.marca;
    }
    public getTipo(){
        return this.tipo;
    }
    public getModelo(){
        return this.modelo;
    }
    public getAnio(){
        return this.anio;
    }
    public getKilometraje(){
        return this.kilometraje;
    }
    public getPatente(){
        return this.patente;
    }
}
